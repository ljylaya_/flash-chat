//
//  Constants.swift
//  Flash Chat iOS13
//
//  Created by Leah Joy Ylaya on 12/15/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Constants {
    static let appName = "⚡️FlashChat"
    static let cellIdentifier = "ReusableCell"
    static let cellNibName = "MessageCell"
    static let chatSegue = "goToChatView"
    
    struct BrandColors {
        static let purple = "purple"
        static let lightPurple = "lightPurple"
        static let blue = "blue"
        static let lightBlue = "lightBlue"
    }
    
    struct ConstantsStore {
        static let collectionName = "messages"
        static let senderField = "sender"
        static let bodyField = "body"
        static let dateField = "date"
    }
}
