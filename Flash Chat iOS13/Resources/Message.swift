//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Leah Joy Ylaya on 12/15/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
